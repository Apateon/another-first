from django.shortcuts import render
from django.http import JsonResponse
import requests
import json

# Create your views here.

def book(request):
    return render(request, 'book.html')

def data(request):
    raw = requests.get("https://www.googleapis.com/books/v1/volumes?q=quilting").json()

    data = [];
    for info in raw['items']:
        item = {}
        item['id'] = info['id']
        item['cover'] = info['volumeInfo']['imageLinks']['smallThumbnail']
        item['title'] = info['volumeInfo']['title']
        item['authors'] = ", ".join(info['volumeInfo']['authors'])
        item['description'] = info['volumeInfo']['description']
        data.append(item)
    return JsonResponse({"data" : data})