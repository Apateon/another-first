$(window).on('load', function () {
    $('#loading').fadeOut(5, function(){
      $('#bottom_div').show();
      $('#main_div').show();
    });
  });
  $(document).ready(function(){     
    getBooks();
});

function getBooks() { 
    $.ajax({
        url:"/book/data/",
        dataType:"json",
        success: function (response) { 
            console.log("success");
            var lists = response["data"];
            var append = "";
            for (i = 0; i < lists.length; i++){
                append+="<tr>";
                append+="<td>"+(i+1)+"</td>";
                append+="<td>"+"<img class='cover-image' src="+lists[i]['cover']+">"+"</td>";//kolom cover
                append+="<td>"+lists[i]['title']+"</td>";//kolom T A D
                append+="<td>"+lists[i]['authors']+"</td>";//kolom T A D 
                append+="<td style='text-align:justify'>"+lists[i]['description']+"</td>";//kolom T A D
                append+="<tr>";
            }
            $('#result').html(append);    
        }

    })
 }
